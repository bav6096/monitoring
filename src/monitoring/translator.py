#!/usr/bin/env python

import rospy

from monitoring.msg import Metric
from monitoring.msg import Monitoring
from monitoring_msgs.msg import MonitoringArray
from monitoring_msgs.msg import MonitoringInfo
from monitoring_msgs.msg import KeyValue


class Translator:
    def __init__(self):
        self.sub = rospy.Subscriber("/luhrts_monitoring", MonitoringArray, self.translate)
        self.pub = rospy.Publisher("/monitoring", Monitoring, queue_size=1)

    def translate(self, event):
        metric = Metric()
        metric.domain = event.info[0].description
        metric.label = event.info[0].values.key
        metric.value = event.info[0].values.value
        metric.unit = event.info[0].values.unit
        metric.error = event.info[0].values.errorlevel

        monitoring = Monitoring()
        monitoring.origin = event.info[0].name
        monitoring.metric = metric

        self.pub.publish(monitoring)
