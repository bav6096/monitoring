#!/usr/bin/env python

import rospy
import socket

from monitoring.msg import Metric
from monitoring.msg import Monitoring


class Monitor:
    """
    Each metric should have its own monitor.
    A monitor has four modes:

    - Mode 1: Publish the last received value.
    - Mode 2: Publish the lowest received value.
    - Mode 3: Publish the highest received value.
    - Mode 4: Publish the average received value
              over five or more seconds.

    Furthermore, a monitor publishes messages with a
    certain frequency.
    """
    def __init__(self, mon_mode=1):
        self.log = {}  # Logging dictionary. Is used to accomplish different publishing modes.
        self.pub = rospy.Publisher("/monitoring", Monitoring, queue_size=1)
        self.mon_mode = mon_mode

        mon_freq = rospy.get_param("/mon_freq")
        if not mon_freq > 0.0:
            rospy.logwarn("The frequency at which a monitor publishes messages must be greater then 0! "
                          "Using 1 as frequency!")
            mon_freq = 1.0

        self.timer = rospy.Timer(rospy.Duration(1.0 / mon_freq), self.publish_monitoring)

        self.monitoring = Monitoring()
        self.monitoring.origin = socket.gethostname() + rospy.get_name()

    # At the moment the last metric update ist published over and over.
    def publish_monitoring(self, _):
        self.pub.publish(self.monitoring)

    def update_metric(self, domain, label, value, unit, error):
        def set_metric():
            metric = Metric()
            metric.domain = str(domain)
            metric.label = str(label)
            metric.value = str(value)
            metric.unit = str(unit)
            metric.error = error

            self.monitoring.metric = metric

        def rst_key():  # Reset the value of a key in the logging dictionary.
            self.log[label] = {"num": 0, "val": 0, "sum": 0, "dur": rospy.get_rostime()}

        if " " in label:  # A label cannot contain whitespace!
            rospy.logwarn("Whitespaces are not allowed in metric labels!")
        elif error > 1 or error < 0:
            rospy.logwarn("Error level must be between 1 and 0!")
        else:
            if label in self.log:
                # Mode 1: Set the last obtained value.
                if self.mon_mode == 1:
                    set_metric()

                # Mode 2: Remember and set the minimum value,
                #         obtained after mode selection.
                elif self.mon_mode == 2:
                    if value < self.log[label]["val"]:
                        self.log[label]["val"] = value
                    else:
                        value = self.log[label]["val"]

                    set_metric()

                # Mode 3: Remember and set the maximum value,
                #         obtained after mode selection.
                elif self.mon_mode == 3:
                    if value > self.log[label]["val"]:
                        self.log[label]["val"] = value
                    else:
                        value = self.log[label]["val"]

                    set_metric()

                # Mode 4: Set the average obtained value over five
                #         or more seconds.
                elif self.mon_mode == 4:
                    duration = rospy.get_rostime() - self.log[label]["dur"]
                    if duration < rospy.Duration(5):
                        self.log[label]["num"] += 1
                        self.log[label]["sum"] += value
                    else:
                        value = self.log[label]["sum"] / (self.log[label]["num"] + 0.001)
                        set_metric()
                        rst_key()
            else:
                rst_key()  # Add key to the logging dictionary.
                set_metric()
